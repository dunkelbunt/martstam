<?php
/**
 * Template Name: Preis Template
 * Template Post Type: post, page
 * 
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive-preistraeger.twig' );

$context = Timber::context();
$post = Timber::query_post();
$context['post'] = $post;

$context['title'] = 'Archive';

$args = array(
    'post_type' => 'preistraeger',
    'orderby' => 'meta_value_num',
    'meta_key' => 'preistraeger-jahr',
    'order'   => 'DESC',
    'posts_per_page'  => 150,
);

$context['posts'] = Timber::get_posts( $args );

Timber::render( $templates, $context );
