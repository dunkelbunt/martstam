( function( wp ) {
    var el = wp.element.createElement;
    var registerBlockType = wp.blocks.registerBlockType;
    var TextControl = wp.components.TextControl;
 
    registerBlockType( 'martstam/preistraeger-name', {
        title: 'Preisträger Name',
        icon: 'smiley',
        category: 'common',
 
        attributes: {
            blockValue: {
                type: 'string',
                source: 'meta',
                meta: 'preistraeger-name',
            },
        },

        supports: {
            inserter: false
        },
        
 
        edit: function( props ) {
            var className = props.className;
            var setAttributes = props.setAttributes;
 
            function updateBlockValue( blockValue ) {
                setAttributes( { blockValue } );
            }
 
            return el(
                'div',
                { className: className },
                el( TextControl, {
                    label: 'Name der Preisträger/innen',
                    value: props.attributes.blockValue,
                    onChange: updateBlockValue,
                } )
            );
        },
 
        // No information saved to the block
        // Data is saved to post meta via attributes
        save: function() {
            return null;
        },
    } );

    registerBlockType( 'martstam/preistraeger-jahr', {
        title: 'Preisträger Jahr',
        icon: 'smiley',
        category: 'common',
 
        attributes: {
            blockValue: {
                type: 'integer',
                source: 'meta',
                meta: 'preistraeger-jahr',
            },
        },
 
        supports: {
            inserter: false
        },

        edit: function( props ) {
            var className = props.className;
            var setAttributes = props.setAttributes;
 
            function updateBlockValue( blockValue ) {
                setAttributes( { blockValue } );
            }
 
            return el(
                'div',
                { className: className },
                el( TextControl, {
                    label: 'Jahr der Preisverleihung',
                    value: props.attributes.blockValue,
                    onChange: updateBlockValue,
                } )
            );
        },
 
        // No information saved to the block
        // Data is saved to post meta via attributes
        save: function() {
            return null;
        },
    } );
} )( window.wp );