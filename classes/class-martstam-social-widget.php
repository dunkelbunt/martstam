<?php

/**
 * Social Widget
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

if (!class_exists('MartStam_Social_Widget')) {
	/**
	 * Separator Control.
	 */
	class MartStam_Social_Widget extends WP_Widget
	{ // Creating the widget 


		// The construct part  
		function __construct()
		{
			$widget_options = array(
				'classname' => 'social_widget',
				'description' => 'Social Media Links',
			);
			parent::__construct('social_widget', 'Social Media', $widget_options);
		}

		// Creating widget front-end
		public function widget($args, $instance)
		{
			?>
			<h2 class="widget-title subheading heading-size-5">
				<?php echo $instance[ 'title' ] ?>
			</h2>
			<?php
			get_template_part('template-parts/social');
		}

		// Creating widget Backend 
		public function form($instance)
		{
			$title = !empty($instance['title']) ? $instance['title'] : '';
?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
				<input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr($title); ?>" />
			</p>
<?php
		}

		// Updating widget replacing old instances with new
		public function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
			return $instance;
		}

		// Class wpb_widget ends here
	}
}
