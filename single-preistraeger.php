<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
// $context['body_class'] = $context['body_class'] . 'page-template-template-cover page-template-templates overlay-header template-cover-php template-cover';

$cover = array();


	// On the cover page template, output the cover header.
	$cover["header_style"]   = '';
	$cover["header_classes"] = '';

	$cover["overlay_style"]   = '';
	$cover["overlay_classes"] = '';

	$image_url = ! post_password_required() ? get_the_post_thumbnail_url( get_the_ID(), 'martstam-fullscreen' ) : '';

	if ( $image_url ) {
		$cover["header_style"]  = ' style="background-image: url( ' . esc_url( $image_url ) . ' );"';
		$cover["header_classes"] = ' bg-image';
	}

	// Get the color used for the color overlay.
	$color_overlay_color = get_theme_mod( 'cover_template_overlay_background_color' );
	if ( $color_overlay_color ) {
		$cover["overlay_style"]  = ' style="color: ' . esc_attr( $color_overlay_color ) . ';"';
	} else {
		$cover["overlay_style"]  = '';
	}

	// Get the fixed background attachment option.
	if ( get_theme_mod( 'cover_template_fixed_background', true ) ) {
		$cover["header_classes"]  .= ' bg-attachment-fixed';
	}

	// Get the opacity of the color overlay.
	$color_overlay_opacity  = get_theme_mod( 'cover_template_overlay_opacity' );
	$color_overlay_opacity  = ( false === $color_overlay_opacity ) ? 80 : $color_overlay_opacity;
	$cover["overlay_classes"] .= ' opacity-' . $color_overlay_opacity;

    $context['cover'] = $cover;

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
