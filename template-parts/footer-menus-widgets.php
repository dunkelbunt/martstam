<?php

/**
 * Displays the menus and widgets at the end of the main element.
 * Visually, this output is presented as part of the footer element.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

$has_sidebar_1 = is_active_sidebar('sidebar-1');

// Only output the container if there are elements to display.
if ($has_sidebar_1) {
?>

	<div class="footer-nav-widgets-wrapper header-footer-group">

		<div class="footer-inner section-inner">

			<?php if ($has_sidebar_1) { ?>

				<aside class="footer-widgets-outer-wrapper" role="complementary">

					<div class="footer-widgets-wrapper">

						<div class="footer-widgets column-one grid-item">
							<div class="header-titles">

								<?php
								// Site title or logo.
								//martstam_site_logo();
								?>

							</div>
							<?php dynamic_sidebar('sidebar-1'); ?>
						</div>

					</div><!-- .footer-widgets-wrapper -->

				</aside><!-- .footer-widgets-outer-wrapper -->

			<?php } ?>

		</div><!-- .footer-inner -->

	</div><!-- .footer-nav-widgets-wrapper -->

<?php } ?>