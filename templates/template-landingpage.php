<?php
/**
 * Template Name: Landing Page Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( 'page-template-template-cover', 'page-template-templatestemplate-cover-php', 'template-cover' ) );
} );
get_header();

?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content-landingpage' );
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
